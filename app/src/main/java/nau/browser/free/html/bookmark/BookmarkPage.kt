/*
 * Copyright 2014 A.C.R. Development
 */
package nau.browser.free.html.bookmark

import nau.browser.free.BrowserApp
import nau.browser.free.R
import nau.browser.free.constant.FILE
import nau.browser.free.database.HistoryItem
import nau.browser.free.database.bookmark.BookmarkRepository
import nau.browser.free.extensions.safeUse
import nau.browser.free.favicon.FaviconModel
import nau.browser.free.utils.ThemeUtils
import android.app.Activity
import android.app.Application
import android.graphics.Bitmap
import android.text.TextUtils
import io.reactivex.Scheduler
import io.reactivex.Single
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter
import javax.inject.Inject
import javax.inject.Named

class BookmarkPage(activity: Activity) {

    @Inject internal lateinit var app: Application
    @Inject internal lateinit var bookmarkModel: BookmarkRepository
    @Inject internal lateinit var faviconModel: FaviconModel
    @Inject @field:Named("database") internal lateinit var databaseScheduler: Scheduler
    @Inject @field:Named("disk") internal lateinit var diskScheduler: Scheduler

    private val folderIcon = nau.browser.free.utils.ThemeUtils.getThemedBitmap(activity, R.drawable.ic_folder, false)

    init {
        BrowserApp.appComponent.inject(this)
    }

    fun createBookmarkPage(): Single<String> = Single.fromCallable {
        cacheIcon(folderIcon, getFaviconFile(app))
        cacheIcon(faviconModel.getDefaultBitmapForString(null), getDefaultIconFile(app))
        buildBookmarkPage(null)

        val bookmarkWebPage = getBookmarkPage(app, null)

        "$FILE$bookmarkWebPage"
    }

    private fun cacheIcon(icon: Bitmap, file: File) = FileOutputStream(file).safeUse {
        icon.compress(Bitmap.CompressFormat.PNG, 100, it)
        icon.recycle()
    }

    private fun buildBookmarkPage(folder: String?) {
        bookmarkModel.getBookmarksFromFolderSorted(folder)
                .concatWith(Single.defer {
                    if (folder == null) {
                        bookmarkModel.getFoldersSorted()
                    } else {
                        Single.just(emptyList())
                    }
                })
                .toList()
                .map { it.flatten().sorted() }
                .subscribeOn(databaseScheduler)
                .observeOn(diskScheduler)
                .subscribe { bookmarksAndFolders ->
                    buildPageHtml(bookmarksAndFolders, folder)
                }
    }

    private fun buildPageHtml(bookmarksAndFolders: List<nau.browser.free.database.HistoryItem>, folder: String?) {
        val bookmarkWebPage = getBookmarkPage(app, folder)

        val builder = BookmarkPageBuilder(faviconModel, app, diskScheduler)

        FileWriter(bookmarkWebPage, false).use {
            it.write(builder.buildPage(bookmarksAndFolders))
        }

        bookmarksAndFolders
                .filter { it.isFolder }
                .forEach { buildBookmarkPage(it.title) }
    }

    companion object {

        /**
         * The bookmark page standard suffix
         */
        const val FILENAME = "bookmarks.html"

        private const val FOLDER_ICON = "folder.png"
        private const val DEFAULT_ICON = "default.png"

        @JvmStatic
        fun getBookmarkPage(application: Application, folder: String?): File {
            val prefix = if (!TextUtils.isEmpty(folder)) {
                "$folder-"
            } else {
                ""
            }
            return File(application.filesDir, prefix + FILENAME)
        }

        private fun getFaviconFile(application: Application): File =
                File(application.cacheDir, FOLDER_ICON)

        private fun getDefaultIconFile(application: Application): File =
                File(application.cacheDir, DEFAULT_ICON)
    }

}
