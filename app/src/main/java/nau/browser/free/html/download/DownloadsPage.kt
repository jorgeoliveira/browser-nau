/*
 * Copyright 2014 A.C.R. Development
 */
package nau.browser.free.html.download

import nau.browser.free.BrowserApp
import nau.browser.free.constant.FILE
import nau.browser.free.database.downloads.DownloadsRepository
import nau.browser.free.preference.UserPreferences
import android.app.Application
import io.reactivex.Single
import java.io.File
import java.io.FileWriter
import javax.inject.Inject

class DownloadsPage {

    @Inject internal lateinit var app: Application
    @Inject internal lateinit var userPreferences: UserPreferences
    @Inject internal lateinit var manager: DownloadsRepository

    init {
        BrowserApp.appComponent.inject(this)
    }

    fun getDownloadsPage(): Single<String> = manager
            .getAllDownloads()
            .map { list ->
                val directory = userPreferences.downloadDirectory

                val downloadPageBuilder = DownloadPageBuilder(app, directory)

                val fileName = getDownloadsPageFile(app)
                FileWriter(fileName, false).use {
                    it.write(downloadPageBuilder.buildPage(requireNotNull(list)))
                }

                return@map fileName
            }
            .map { "$FILE$it" }

    companion object {

        const val FILENAME = "downloads.html"

        private fun getDownloadsPageFile(application: Application): File =
                File(application.filesDir, FILENAME)
    }

}
