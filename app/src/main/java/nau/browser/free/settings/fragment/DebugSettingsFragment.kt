package nau.browser.free.settings.fragment

import nau.browser.free.BrowserApp
import nau.browser.free.R
import nau.browser.free.preference.DeveloperPreferences
import android.os.Bundle
import javax.inject.Inject

class DebugSettingsFragment : AbstractSettingsFragment() {

    @Inject internal lateinit var developerPreferences: DeveloperPreferences

    override fun providePreferencesXmlResource() = R.xml.preference_debug

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BrowserApp.appComponent.inject(this)

        togglePreference(
                preference = LEAK_CANARY,
                isChecked = developerPreferences.useLeakCanary,
                onCheckChange = { change ->
                    activity?.let {
                        nau.browser.free.utils.Utils.showSnackbar(it, R.string.app_restart)
                    }
                    developerPreferences.useLeakCanary = change
                }
        )
    }

    companion object {
        private const val LEAK_CANARY = "leak_canary_enabled"
    }
}
