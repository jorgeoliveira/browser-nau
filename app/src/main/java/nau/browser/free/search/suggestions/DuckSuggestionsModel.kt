package nau.browser.free.search.suggestions

import nau.browser.free.R
import nau.browser.free.constant.UTF8
import nau.browser.free.database.HistoryItem
import nau.browser.free.extensions.map
import nau.browser.free.utils.FileUtils
import android.app.Application
import org.json.JSONArray
import org.json.JSONObject
import java.io.InputStream

/**
 * The search suggestions provider for the DuckDuckGo search engine.
 */
class DuckSuggestionsModel(application: Application) : BaseSuggestionsModel(application, UTF8) {

    private val searchSubtitle = application.getString(R.string.suggestion)

    override fun createQueryUrl(query: String, language: String): String =
            "https://duckduckgo.com/ac/?q=$query"

    @Throws(Exception::class)
    override fun parseResults(inputStream: InputStream): List<nau.browser.free.database.HistoryItem> {
        val content = nau.browser.free.utils.FileUtils.readStringFromStream(inputStream, UTF8)
        val jsonArray = JSONArray(content)

        return jsonArray
                .map { it as JSONObject }
                .map { it.getString("phrase") }
                .map { nau.browser.free.database.HistoryItem("$searchSubtitle \"$it\"", it, R.drawable.ic_search) }
    }

}
