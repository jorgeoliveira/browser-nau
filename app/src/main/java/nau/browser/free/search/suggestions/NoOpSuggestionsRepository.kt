package nau.browser.free.search.suggestions

import nau.browser.free.database.HistoryItem
import io.reactivex.Single

/**
 * A search suggestions repository that doesn't fetch any results.
 */
class NoOpSuggestionsRepository : SuggestionsRepository {

    private val emptySingle: Single<List<nau.browser.free.database.HistoryItem>> = Single.just(listOf())

    override fun resultsForSearch(rawQuery: String) = emptySingle
}