package nau.browser.free.search.suggestions

import nau.browser.free.R
import nau.browser.free.constant.UTF8
import nau.browser.free.database.HistoryItem
import android.app.Application
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.InputStream

/**
 * Search suggestions provider for Google search engine.
 */
class GoogleSuggestionsModel(application: Application) : BaseSuggestionsModel(application, UTF8) {

    private val searchSubtitle = application.getString(R.string.suggestion)

    override fun createQueryUrl(query: String, language: String): String =
            "https://suggestqueries.google.com/complete/search?output=toolbar&hl=$language&q=$query"

    @Throws(Exception::class)
    override fun parseResults(inputStream: InputStream): List<nau.browser.free.database.HistoryItem> {
        parser.setInput(inputStream, UTF8)

        val mutableList = mutableListOf<nau.browser.free.database.HistoryItem>()
        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG && "suggestion" == parser.name) {
                val suggestion = parser.getAttributeValue(null, "data")
                mutableList.add(nau.browser.free.database.HistoryItem("$searchSubtitle \"$suggestion\"", suggestion, R.drawable.ic_search))
            }
            eventType = parser.next()
        }

        return mutableList
    }

    companion object {

        private val parser by lazy {
            val factory = XmlPullParserFactory.newInstance()
            factory.isNamespaceAware = true

            factory.newPullParser()
        }

    }
}
