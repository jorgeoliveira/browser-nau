package nau.browser.free.search.suggestions

import nau.browser.free.R
import nau.browser.free.constant.UTF8
import nau.browser.free.database.HistoryItem
import nau.browser.free.extensions.map
import nau.browser.free.utils.FileUtils
import android.app.Application
import org.json.JSONArray
import java.io.InputStream

/**
 * The search suggestions provider for the Baidu search engine.
 */
class BaiduSuggestionsModel(
        application: Application
) : BaseSuggestionsModel(application, UTF8) {

    private val searchSubtitle = application.getString(R.string.suggestion)
    private val inputEncoding = "GBK"

    // see http://unionsug.baidu.com/su?wd=encodeURIComponent(U)
    // see http://suggestion.baidu.com/s?wd=encodeURIComponent(U)&action=opensearch
    override fun createQueryUrl(query: String, language: String): String =
            "http://suggestion.baidu.com/s?wd=$query&action=opensearch"

    @Throws(Exception::class)
    override fun parseResults(inputStream: InputStream): List<nau.browser.free.database.HistoryItem> {
        val content = nau.browser.free.utils.FileUtils.readStringFromStream(inputStream, inputEncoding)
        val responseArray = JSONArray(content)
        val jsonArray = responseArray.getJSONArray(1)

        return jsonArray
                .map { it as String }
                .map { nau.browser.free.database.HistoryItem("$searchSubtitle \"$it\"", it, R.drawable.ic_search) }
    }

}
