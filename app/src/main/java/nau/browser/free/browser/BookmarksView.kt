package nau.browser.free.browser

import nau.browser.free.database.HistoryItem

interface BookmarksView {

    fun navigateBack()

    fun handleUpdatedUrl(url: String)

    fun handleBookmarkDeleted(item: nau.browser.free.database.HistoryItem)

}
