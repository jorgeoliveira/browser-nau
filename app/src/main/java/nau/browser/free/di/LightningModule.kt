package nau.browser.free.di

import nau.browser.free.adblock.whitelist.SessionWhitelistModel
import nau.browser.free.adblock.whitelist.WhitelistModel
import nau.browser.free.database.bookmark.BookmarkDatabase
import nau.browser.free.database.bookmark.BookmarkRepository
import nau.browser.free.database.downloads.DownloadsDatabase
import nau.browser.free.database.downloads.DownloadsRepository
import nau.browser.free.database.history.HistoryDatabase
import nau.browser.free.database.history.HistoryRepository
import nau.browser.free.database.whitelist.AdBlockWhitelistDatabase
import nau.browser.free.database.whitelist.AdBlockWhitelistRepository
import nau.browser.free.ssl.SessionSslWarningPreferences
import nau.browser.free.ssl.SslWarningPreferences
import dagger.Binds
import dagger.Module

/**
 * Dependency injection module used to bind implementations to interfaces.
 */
@Module
abstract class LightningModule {

    @Binds
    abstract fun provideBookmarkModel(bookmarkDatabase: BookmarkDatabase): BookmarkRepository

    @Binds
    abstract fun provideDownloadsModel(downloadsDatabase: DownloadsDatabase): DownloadsRepository

    @Binds
    abstract fun providesHistoryModel(historyDatabase: HistoryDatabase): HistoryRepository

    @Binds
    abstract fun providesAdBlockWhitelistModel(adBlockWhitelistDatabase: AdBlockWhitelistDatabase): AdBlockWhitelistRepository

    @Binds
    abstract fun providesWhitelistModel(sessionWhitelistModel: SessionWhitelistModel): WhitelistModel

    @Binds
    abstract fun providesSslWarningPreferences(sessionSslWarningPreferences: SessionSslWarningPreferences): SslWarningPreferences

}