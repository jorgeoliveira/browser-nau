package nau.browser.free.di

import nau.browser.free.BrowserApp
import nau.browser.free.adblock.AssetsAdBlocker
import nau.browser.free.adblock.NoOpAdBlocker
import nau.browser.free.browser.BrowserPresenter
import nau.browser.free.browser.SearchBoxModel
import nau.browser.free.browser.TabsManager
import nau.browser.free.browser.activity.BrowserActivity
import nau.browser.free.browser.activity.ThemableBrowserActivity
import nau.browser.free.browser.fragment.BookmarksFragment
import nau.browser.free.browser.fragment.TabsFragment
import nau.browser.free.dialog.LightningDialogBuilder
import nau.browser.free.download.DownloadHandler
import nau.browser.free.download.LightningDownloadListener
import nau.browser.free.html.bookmark.BookmarkPage
import nau.browser.free.html.download.DownloadsPage
import nau.browser.free.html.history.HistoryPage
import nau.browser.free.html.homepage.StartPage
import nau.browser.free.network.NetworkConnectivityModel
/*
import nau.browser.free.reading.activity.ReadingActivity
*/
import nau.browser.free.search.SearchEngineProvider
import nau.browser.free.search.SuggestionsAdapter
import nau.browser.free.settings.activity.ThemableSettingsActivity
import nau.browser.free.settings.fragment.*
import nau.browser.free.utils.ProxyUtils
import nau.browser.free.view.LightningChromeClient
import nau.browser.free.view.LightningView
import nau.browser.free.view.LightningWebClient
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (LightningModule::class)])
interface AppComponent {

    fun inject(activity: BrowserActivity)

    fun inject(fragment: BookmarksFragment)

    fun inject(fragment: BookmarkSettingsFragment)

    fun inject(builder: LightningDialogBuilder)

    fun inject(fragment: TabsFragment)

    fun inject(lightningView: LightningView)

    fun inject(activity: ThemableBrowserActivity)

    fun inject(advancedSettingsFragment: AdvancedSettingsFragment)

    fun inject(app: BrowserApp)

    fun inject(proxyUtils: nau.browser.free.utils.ProxyUtils)

//    fun inject(activity: ReadingActivity)

    fun inject(webClient: LightningWebClient)

    fun inject(activity: ThemableSettingsActivity)

    fun inject(listener: nau.browser.free.download.LightningDownloadListener)

    fun inject(fragment: PrivacySettingsFragment)

    fun inject(startPage: StartPage)

    fun inject(historyPage: HistoryPage)

    fun inject(bookmarkPage: BookmarkPage)

    fun inject(downloadsPage: DownloadsPage)

    fun inject(presenter: BrowserPresenter)

    fun inject(manager: TabsManager)

    fun inject(fragment: DebugSettingsFragment)

    fun inject(suggestionsAdapter: SuggestionsAdapter)

    fun inject(chromeClient: LightningChromeClient)

    fun inject(downloadHandler: nau.browser.free.download.DownloadHandler)

    fun inject(searchBoxModel: SearchBoxModel)

    fun inject(searchEngineProvider: SearchEngineProvider)

    fun inject(generalSettingsFragment: GeneralSettingsFragment)

    fun inject(displaySettingsFragment: DisplaySettingsFragment)

    fun inject(networkConnectivityModel: NetworkConnectivityModel)

    fun provideAssetsAdBlocker(): AssetsAdBlocker

    fun provideNoOpAdBlocker(): NoOpAdBlocker

}
