package nau.browser.mynau;

import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import nau.browser.free.R;

public class Terms extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        WebView wv = findViewById(R.id.webview);
        wv.setWebViewClient(new Terms.SSLTolerentWebViewClient());
        wv.loadUrl("https://www.nau.mobi/terms_mobile");

        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);

        Button acceptTerms = findViewById(R.id.accept_terms);
        acceptTerms.setOnClickListener(view -> {
            finish();
        });

        Button disagreeTerms = findViewById(R.id.disagree_terms);
        disagreeTerms.setOnClickListener(view -> finish());
    }

    private class SSLTolerentWebViewClient extends WebViewClient {
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }
    }

}
