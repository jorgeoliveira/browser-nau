package nau.browser.free

/**
 * The SDK version that should be used by robolectric.
 */
const val SDK_VERSION = 27